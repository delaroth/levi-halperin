const ids = [   
]
// const tasks = [ 
//     {
//         id : idGen(),
//         text : "build toDoList app",
//         completed : false
//     },
//     {
//         id : idGen(),
//         text : "Do grocery shopping",
//         completed : false
//     },
//     {
//         id : idGen(),
//         text : "finish tictactoe game",
//         completed : true
//     }
// ]



function idGen() {
    let id = Math.floor(Math.random() * 1000 + 1)
    while (ids.includes(id)) {
        id = Math.floor(Math.random() * 1000 + 1)
    }
    ids.push(id)
    return id 
}

function renderList () {
    
    let uncompleted = ""
    let completed = ""
    console.log("hi")
    axios.get(`http://localhost:3000/toDo`)
    .then(res=>{
        const tasks = res.data

        tasks.forEach( 
            (item) => {
                if (!item.completed) {
                    uncompleted += `<li class="task" id="${item.id}"> <span>${item.text}</span> <button class="delete" onclick="deleteTask(${item.id})" > X </button> 
                    <input class="checkbox" onchange="updateTask('${item.id}')" type="checkbox"></li>`
                }
                else {
                    completed += 
                    `<li class="task completed" id="${item.id}"> <span>${item.text}</span>  
                    <input class="completed" onchange="updateTask('${item.id}')" type="checkbox" checked="true"></li>`
                }   
            } 
            )
            
            document.getElementById("uncompleted").innerHTML = uncompleted
            document.getElementById("completed").innerHTML = completed
        })

}


// adds task and renders list when submited



    
document.querySelector('form').onsubmit = event => {
    event.preventDefault()
    const values = Object.values(event.target)
        .reduce((acc, input) => !input.name ? acc : ({
            ...acc,
            [input.name]: input.type == 'checkbox' ? input.checked : input.value
        }), {}
        )
    console.log(values)

    addNewTasks (values.addTask)

}
//




function addNewTasks (task) {

    axios.post('http://localhost:3000/toDo', {todo: `${task}`})
    .then(res=>{ renderList()})
                            }



function updateTask(id) {

    axios.put('http://localhost:3000/toDo', {id: `${id}`})
    .then(res => {
      renderList()  
    })
}


function deleteTask (id) {
    axios.delete(`http://localhost:3000/toDo/${id}`)
.then(res => {
    renderList()
})
}


renderList()
