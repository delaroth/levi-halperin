const { query } = require('express')
const express = require('express')
const cors = require('cors')
const app = express()
 
app.use(cors())
app.use(express.json())

const ids = [145, 123, 11]
function idGen() {
    let id = Math.floor(Math.random() * 1000 + 1)
    while (ids.includes(id)) {
        id = Math.floor(Math.random() * 1000 + 1)
    }
    ids.push(id)
    return id 
}

const tasks = [ 
    {
        id : 145,
        text : "build toDoList app",
        completed : false
    },
    {
        id : 123,
        text : "Do grocery shopping",
        completed : false
    },
    {
        id : 111,
        text : "finish tictactoe game",
        completed : true
    }
]

app.get('/toDo', function(req,res) {
     res.send(tasks)   
})



// app.get('/toDo/:id', function(req,res) {
//     if (!isNaN(req.params.id)) {
//         tasks.forEach(item => {
//             if(item.id == req.params.id) {
//                 res.send("id found")
//                 res.send(`id:${item.id} todo:${item.text} done:${item.completed}`)
//             }
//         }
//         )
        
//     } 
//     else {
//      res.send("id not found")
//      res.send(tasks)   
//     }
// })


// app.get('/toDo/:id', function(req,res) {

//         let task = tasks.find((item, index) => {
//                    if(item.id == req.params.id)   {
//                     return true
//                                                   }
//                                                 }
                                                
//         () => {
//             if(task) {
//               res.send("id found")
//               res.send(`id:${task.id} todo:${task.text} done:${task.completed}`)  
//             }
            
//             else {
//              res.send("id not found")
//              res.send(tasks)   
//             }}
//             )
//         }
        
// )
    


app.post('/toDo', function(req,res) {
    if (req.body.todo) {
      tasks.push( 
            {
              id: idGen(),
              text: req.body.todo,
              completed: false
            }) 
            res.send("done") 
    }

    else {
        res.send("please use the key 'todo' to add to the todo list")
    }
    
            
            
} )

app.put('/toDo', function(req,res) {
    if (!isNaN(req.body.id))  {
     tasks.forEach((item, index) => {
     if (item.id == req.body.id) {
         tasks[index].completed = !tasks[index].completed
     }
     }
     )
     res.send("done")   
    }
    res.send("please use the key 'id' and give it the value of a number")
    }
    )

app.delete('/toDo/:id', function(req,res) {
    if (!isNaN(req.params.id)) {
      tasks.forEach( (item, index) => {
        if (item.id == req.params.id) {
            tasks.splice(index, 1)
            res.send("done")   
                            }
                                     }
                   )  
    }
    else {res.send("id not found")}
                                           }
            )
    
     
app.listen(3000, () => {
    
console.log("found")    
})


